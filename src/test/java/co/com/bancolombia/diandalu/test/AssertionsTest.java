package co.com.bancolombia.diandalu.test;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;

import org.junit.Test;

public class AssertionsTest {

	@Test
	public void assertionsTest(){
		assertThat("123").isEqualTo("123");
		assertThat(true).isTrue();
		assertThat(new File("noExiste.txt")).doesNotExist();
	}
}
