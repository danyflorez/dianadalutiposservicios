package co.com.bancolombia.diandalu.services;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
//Path Param
//Query Param
//Objetos

import co.com.bancolombia.diandalu.dto.User;

@Path("/practice")
public class PracticeService {

	@GET
	@Path("getMethod/{nombre}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getMethod(@PathParam("nombre") String nombre){
		return "Hello " + nombre;
	}
	
	@DELETE
	@Path("deleteMethod")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteMethod(@QueryParam("userName") String userName){
		return "Delete " + userName;
	}
	
	@PUT
	@Path("putMethod")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User putMethod(User user){
		user.setUserName("Recibido");
		user.setPassword("123");
		return user;
	}
	
	@POST
	@Path("postMethod")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User postMethod(User user){
		user.setUserName("Recibido por post");
		user.setPassword("123");
		return user;
	}
	
	
	
}
